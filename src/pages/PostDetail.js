import React, { Component } from 'react'
import Header from '../components/header/Header'
import CardDetail from '../components/card-detail/CardDetail'
import CardComment from '../components/card-comment/CardComment'

class PostDetail extends Component {
    state = {
        title: "",
        body: "",
        comments: []
    }

    getPostDetail = async (id) => {
        const URL_POST = `https://jsonplaceholder.typicode.com/posts/${id}`
        const URL_COMMENT = `https://jsonplaceholder.typicode.com/comments?postId=${id}`
        await Promise.all([
            fetch(URL_POST),
            fetch(URL_COMMENT)
        ]).then((allResponse) => {
            allResponse[0].json().then(j => this.setState({ title: j.title, body: j.body }))
            allResponse[1].json().then(j => this.setState({ comments: j }))
        })
    }
    renderComments = () => {
        const { comments } = this.state
        return comments.map((comment) => {
            return (
                <CardComment key={comment.id} name={comment.name} email={comment.email} body={comment.body} />
            )
        })
    }
    UNSAFE_componentWillMount() {
        const { params } = this.props.match
        this.getPostDetail(params.id)
    }
    render() {
        const { title, body } = this.state
        return (
            <div>
                <Header title="SayHi!" />
                <div className="container" style={{ "marginTop": "5%" }}>
                    <CardDetail title={title} body={body} />
                    <br />
                    <br />
                    <h4>Comments</h4>
                    {this.renderComments()}
                </div>
            </div>
        )
    }
}

export default PostDetail;