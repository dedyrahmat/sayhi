import React from "react";

export default function CardComment({ name, email, body }) {
  return (
    <div className="card" style={{'marginBottom': '5px'}}>
      <div className="card-body">
        <blockquote className="blockquote mb-0">
          <p style={{'fontSize': '15px'}}>{body}</p>
          <footer style={{'paddingTop': '0', 'paddingBottom': '0'}} className="blockquote-footer">
            By {name} <cite>({email})</cite>
          </footer>
        </blockquote>
      </div>
    </div>
  );
}
