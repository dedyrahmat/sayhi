import React from 'react'

export default function Card({ title, body, postId }) {
    return (
        <div className="card">
            <div className="card-body">
                <h5 className="card-subtitle mb-2 text-muted">{title}</h5>
                <p className="card-text">{body}</p>
                <a href={"/post/" + postId} className="card-link">Comment</a>
            </div>
        </div>
    )
}