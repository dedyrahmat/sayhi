import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import PostDetail from './pages/PostDetail';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'

const NotFound = () => <h1>Page Not Found</h1>
const routing = (
    <Router>
        <div>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/post/:id" component={PostDetail} />
                <Route component={NotFound} />
            </Switch>
        </div>
    </Router>
);


ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
