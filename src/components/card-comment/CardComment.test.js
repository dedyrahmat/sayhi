import React from 'react';
import ReactDOM from 'react-dom';
import CardComment from './CardComment';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CardComment />, div);
    ReactDOM.unmountComponentAtNode(div);
});