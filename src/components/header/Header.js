import React from "react";

export default function Header({ title }) {
    return (
        <header>
            <div className="navbar navbar-dark bg-dark shadow-sm">
                <div className="container d-flex justify-content-between">
                    <a href="/" className="navbar-brand d-flex align-items-center"><strong>{title}</strong></a>
                </div>
            </div>
        </header>
    );
}
