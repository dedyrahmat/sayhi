import React from 'react'

export default function Card({ title, body }) {
    return (
        <div className="card">
            <div className="card-body">
                <h3 style={{'marginBottom': '25px'}}>Post</h3>
                <h5 className="card-subtitle mb-2 text-muted">{title}</h5>
                <p className="card-text">{body}</p>
            </div>
        </div>
    )
}