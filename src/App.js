import React, { Component } from 'react';
import './App.css';

import Header from './components/header/Header'
import Card from './components/card/Card'


class App extends Component {
  state = {
    posts: []
  }
  getPosts = async () => {
    await fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((json) => {
      this.setState({ posts: json })
    })
  }
  renderPosts = () => {
    const { posts } = this.state
    return posts.map((post) => {
      return (
        <div key={post.id} className="col-6" style={{ 'marginBottom': '15px' }}>
          <Card postId={post.id} title={post.title} body={post.body} />
        </div>
      )
    })
  }

  UNSAFE_componentWillMount() {
    this.getPosts()
  }

  render() {
    return (
      <div>
        <Header title="SayHi!" />
        <section className="jumbotron text-center">
          <div className="container">
            <h1 className="jumbotron-heading">SayHi!</h1>
            <p className="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
          </div>
        </section>

        <div className="album py-5 bg-light">
          <div className="container">
            <div className="row">
              {
                this.renderPosts()
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
